**Documentation**

Les librairies utilisées: 
  - dplyr
  - tidyverse
  - magrittr
  - MASS
  - plotly
  - made4
  
Les fichiers utilisés: 
  - Protein.tsv
  - Gene.tsv
  - Conserved_domain.tsv
 -> Ils sont conservés dans un dossier, le même que le script R pour la création de la matrice.


Première étape : Configuration de l'environnement 
Tout d'abord, il faut se placer dans le répertoire contenant nos fichiers. Pour se faire :

  _  Session -> Set Working Directory -> Choose Directory _

Il faut en suite charger toute les librairies. 

L'option 
  _  options(scipen = 999)_
à été ajouter afin de ne plus avoir les identifiants des gènes sous forme de puissance de 10.

Deuxième étape : Construction de la matrice 

1- Gestion de la table Gene
Cette table à été gardée afin de rajouter des not_ABC (individus qui ne code pas pour un partenaire de systeme ABC ) afin d'améliorer la prédiction d'ABC ou non lors de la classification. Pour cela, chaque individu de la table gene à été classé comme étant not_ABC, puis les lignes ont été comparées avec les lignes de la table protéine. En effet, dans la table protéine, il n'y a que des gènes codant pour des partenanires du systemes ABC. Si un individu se trouve à la fois dans gene et dans protéine, il est donc lié aux transporteurs ABC. Cette ligne sera alors supprimée de notre table gene. 

2- Filtre pour la diminution du nombre d'observation sur la table gene.
L'utilisation du Self_score (score d'alignement d'une sequence sur elle même) nous permet de réaliser un premier filtrage. La moyenne dans notre table est de 832, nous supprimons ceux ayant un Self_score inférieur à 800.

L'ajout d'une colonne longueur correspondant à la taille de notre séquence est ajoutée. Le taille moyenne des gènes bactérien est de 950 nucléotides (chez E.coli), pour encore réduire notre nombre d'observation, on garde les gènes d'une longueur comprise entre 600 et 1300 nucléotides. 

Nous obtenons, au final, un total de 13 076 observations, soit de not_ABC. 

3- Filtrage de la table Protein
Pour cette table , nous avons choisis de garder les protéines avec le status d'identification "Confirmed".

4- Fusion de la table gene et Protein
Réalisation d'un merge en regroupant par les variables Gene_ID, Type, Chromosome et Seq_name afin d'éviter les doublons de variables. 
La fonction merge permet de concaténer les deux tables et ainsi obtenir une matrice contenant l'ensemble des observations choisis. 

5 - Suppression des doublons dans la table domaine_conserve
Supression des doublons Gene_ID par filtrage a l'aide de la e_value. Les individus conservés auront les e_values les plus faibles.Il restera 342 026 observations. 

6 - Création de la colonne contenant les types ABC et not_ABC

7- Suppression des colonnes considérées comme inutiles à la classification 

8- Création d'un fichier de sortie Table.csv
Le fichier de sortie est une matrice individu en ligne et variable en colonne. 
