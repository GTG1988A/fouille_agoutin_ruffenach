# Contexte

Les nouvelles technologies durant les dernières décennies ont conduit aux Big data. Cela touche un grand nombre de domaines, et la biologie n’en échappe pas. Depuis quelques années, les nouvelles technologies de séquençage sont de plus en plus utilisées, et surtout, de plus en plus efficaces. Le résultat est une masse de données gigantesque qu’il faut traiter. 

L’humain n’étant pas assez rapide et efficace, il faut alors créer des outils capables d’effectuer ces tâches. C’est le concept du Deep Learning. Le but final est de créer un algorithme qui apprend d’exemples pour prédire ce que l’on souhaite. 

Dans ce projet, nous nous trouvons dans le cadre d’une fouille de données. En effet, nous disposons d’une grande masse de données annotées sur les systèmes ABC. Elles se trouvent dans une banque de données publique ABCdb maintenue à Toulouse. Elle a été constituée à partir de l’expertise d’une centaine de génomes complets.

Les systèmes ABC forment une très grande famille multigénique. Ce sont des transporteurs membranaires dont le rôle est de transporter diverses substances tel les ions. Ils hydrolysent l’ATP  pour réaliser le transport de part et d’autre de la membrane cytoplasmique. Ces systèmes sont de deux types : Exporteur et Importeur.


![Transporteur ABC](transporteurs.png)

Leur architecture est bien conservée et chaque domaine répond à un rôle .Les domaines MSD forment le pore à travers la membrane, les domaine NBD fournissent l’énergie pour le transport actif par hydrolyse de l’ATP et les SBP capture le substrat et le mène à l’entrée du pore. 

Le but du projet est de mettre au point et d'évaluer une méthode pour l’annotation du génome complet de procaryotes en termes de systèmes ABC. Pour cela, il va falloir utiliser les concepts abordés en fouille de données. 


# Analyse

L'objectif de ce projet consiste à créer un classificateur capable de déterminer si un gène code ou non pour un partenaire d’un système ABC. Notre classificateur devra permettre l’annotation automatique d’une nouvelle donnée qui ne serait pas encore étudiée pour le système ABC.  Pour cela, il faut suivre scrupuleusement les étapes du data mining. La fouille de données suit plusieurs étapes. Les premières concernent l’apprentissage du domaine d’application et la création du jeu de donnée cible, mais ces étapes sont déjà réalisées et nous disposons de la base de données. Dans celle-ci figure :
- Les génomes complets : table Strain et Chromosome
- La classification taxonomique des génomes : table Taxonomy
- les gènes de chaque génome : table gene
- les domaines identifiés sur les gènes : table conserved_Domain et Functional_Domain
- les relations d’orthologie 1:1 : table Orthology
- les gènes identifiés comme ABC : table Protein 
- Les systèmes réassemblés : Table assembly. 


Il faut donc passer aux étapes suivantes qui consisteront en la création des jeux de données cibles, le nettoyage et prétraitement des données et enfin la réduction et transformation des données. Pour cela une des plus grosses étapes du projet va être la création d’une matrice individus-variables à l’aide de R. La création de cette matrice est détaillée dans la partie conception de ce rapport ainsi que dans le répertoire data.préparation sur gitlab.


La création de cette matrice a impliqué la sélection des données nécessaires qui pourrait être intéressante pour classifier nos domaines. Nous avons choisis de garder :
- La table protein : Nous considérons que les éléments de cette table sont des ABC.
- La table gene : Afin d’avoir un pool de not_ABC pour la classification
- la table Conserved_Domain : Les variables qu’il y a dedans semblaient intéressantes pour la classification et c’est un jeu de donnés avec beaucoup de Gene_ID, donc qui contenait sûrement tous les Gene_ID nécessaires à notre fusion gene et protein. 


Pour réussir à choisir les variables que nous allons garder permettant ainsi de classer nos individus, nous avons effectué une Analyse préliminaire avec R ( le script de cette analyse préliminaire se trouve à la suite de la création de la matrice). Avant celle-ci, nous avions déjà réfléchi et supprimé une majorité des variables jugées “non intéressantes” et conservé certaines dont les valeurs pouvaient, au premier abord, permettre une bonne classification. Les variables d’intérêt sont les suivantes:
- CD_ID,FD_ID,Q_STAR,Q_END,S_STAR,S_END de la table Conserved_Domain
- Chromosomes , Seq_Name de la table Protein. 
 
Après avoir tester leurs indépendances et leurs répartition en fonction des deux classes, nous avons supprimé Seq_Name et CD_ID dû au fait que l’hypothèse d'indépendance n’est pas vérifiée ainsi que Chromosome qui semble avoir autant d’ABC que de not ABC sur certaine valeurs, ce qui ne serait pas pertinent pour une classification (50% d’erreur pour cette variable pourrait engendrer une augmentation du taux d’erreur lors de la classification et la prédiction de classe). Nous avons aussi pu déduire que les variables qui semblaient les plus explicatives étaient Q_END et FD_ID.

Une fois cette matrice créée, nous avons utilisé le logiciel Knime pour la construction du modèle et l’évaluation de ces performances par validation croisée ou partitionnement 1/3 - 2/3 . Pour cela, nous avons choisis comme algorithme les approches Knn, Decision tree et Random forest. 

# Conception

    a) Creation de la matrice (voir aussi data.preparation)

        1- Gestion de la table Gene

Cette table à été conservée afin de rajouter des individus de la classe not_ABC (individus qui ne code pas pour un partenaire du système ABC ) afin de pouvoir réaliser la prédiction d'ABC ou non lors de la classification. En effet, lors d’une classification, si les classes ne contiennent pas un nombre d’individus proche (de l’ordre de 90%-10% par exemple) alors, il est “facile” de réaliser une classification puisque le classificateur ne prédira qu’une seule classe. Pour cela, chaque individu de la table Gene a été classé comme étant not_ABC, puis les lignes ont été comparées avec les lignes de la table Proteine. En effet, la table Proteine ne contient que des partenaires aux systèmes ABC, il est donc pertinent de comparer ces 2 tables pour supprimer de la table Gene les observations étant déjà présentes dans la table Proteine et cela dans le but de ne conserver, dans la table Gene, que les observations étant “réellement” des gènes non liés aux systèmes ABC. 
 
        2- Filtre pour la diminution du nombre d'observations sur la table gene.

Après suppression des observations communes entre les tables Proteine et Gene, nous devions réduire le nombre d’individus qui été relativement grand dans le but d’avoir environ autant d'individus “not_ABC” que d’individus ABC ...
Pour cela, l'utilisation du Self_score (score d'alignement d'une séquence sur elle-même) nous permet de réaliser un premier filtrage. La moyenne de ce score étant de 832 dans notre table, nous pouvons donc filtrer les observations ayant un Self_score inférieur à 800.

Cependant, un seul filtre ne permet pas d’obtenir autant d’observations qu’il y en a dans la table Proteine afin de réaliser une bonne classification.
Pour cela, une variable à été créée pour représenter la longueur du gène en utilisant le début et la fin du gène. Toutefois, les gènes pouvant se trouver sur le brin complémentaire, cette longueur à été calculée à l’aide de la fonction “abs” pour avoir une valeur absolue et ainsi, ne contenir que des longueurs positives.
De plus, la taille moyenne des gènes bactériens est de 950 nucléotides (chez E.coli), le second filtrage consiste à ne conserver que les gènes ayant une longueur comprise entre 600 et 1300 nucléotides. 
Nous obtenons, au final, un total de 13 076 observations ne faisant pas parties du système ABC (et ce nombre est proche du non d’observations impliquées dans les systèmes ABC). Donc, nous avons environ le même nombre d’observations qui sont des ABC et not_ABC, permettant ainsi de continuer la matrice utilisée ensuite pour la classification.
 

        3- Filtrage de la table Protein

Pour cette table , nous avons choisis de garder les protéines avec le statut d'identification "Confirmed" pour ne conserver que les observations “fiables” dont le statut à été confirmé.
 
        4- Fusion de la table gene et Protein

Pour réaliser la matrice finale, il est nécessaire de fusionner les tables à l’aide de la fonction merge en fusionnant ces dernières par des variables communes pour éviter la “duplication” de variables mais aussi que les observations n’aient pas de “NA” pour ces variables. Les variables communes sont les suivantes: Gene_ID, Type, Chromosome et Seq_name.
La fonction merge permet de concaténer les deux tables et ainsi obtenir une matrice contenant l'ensemble des observations choisies.
 
        5 - Suppression des doublons dans la table domaine_conserve

Comme énoncé précédemment, nous allons utilisé les variables de Domaine_Conserve pour la classification. Or, cette table contient de nombreuses observations qui ne sont pas présentes dans notre matrice.
Nous allons donc réaliser une suppression des doublons des Gene_ID par filtrage à l'aide de la e_value, c’est-à-dire qu’au sein des observations ayant le même Gene_ID (et donc au sein des doublons), seule l’observation avec la plus petite e_value sera conservée.
 
        6 - Création de la colonne contenant les types ABC et not_ABC : Presence

Ayant encore des données comportant une valeur autre que “ABC” et “not_ABC” dans notre matrice, nous avons décidé de créer une colonne “Presence” qui correspond aux 2 classes citées précédemment. La prédiction consistera alors uniquement à prédire si une observation, en fonction de certaines données, est liée aux systèmes ABC ou non.
 
        7- Suppression des colonnes considérées comme inutiles à la classification 

De part la fusion entre les tables Proteine et Gene, l’ajout des variables Proteine sur les observations de Gene  (et inversement) induit la présence de “NA”, potentiellement problématiques lors de la classification.
De plus, les variables contenues dans “Domaine_cConserve” semblent très intéressantes pour la classification.
Ainsi, nous avons réalisé une suppression des variables contenues dans les 2 tables (Proteine et Gene) pour une meilleure classification.

        8- Création d'un fichier de sortie Table.csv

Le fichier de sortie est une matrice individus en ligne et variable en colonne. Cette matrice sera donc utilisée par la suite pour réaliser la classification (mais aussi la prédiction).

 
    b) Knime : Paramétrage de nos modèles. 

 1) Création des modèles

Dans cette partie, nous avons testé les quatres méthodes de classification connues : 
- Classificateur Bayésien naïf : une méthode d’apprentissage probabiliste qui calcul explicitement les probabilités des hypothèses. Elle est basée sur le théorème de Bayes. Le but est d’attribuer à un objets X une classe C qui maximise P(C|X). 

- Arbre de décision : Construction d’un arbre ou les nœuds internes sont des tests sur les attributs, les branches sont les valeurs des attributs et les feuilles les classes. Au départ, tous les exemples du jeu d’apprentissage sont à la racine, puis il y a partitionnement récursif des exemples en sélectionnant des attributs. Nous classifions donc nos individus (exemple)  en fonction de nos variables (attributs). 

- forêt aléatoire : L’algorithme est le même que pour l’arbre de décision, mais permet l'échantillonnage des attributs pour décorréler les arbres construits. Il lutte contre la sensibilité et le sur-apprentissage d’un arbre de décision seul.

- K plus proches voisins : C’est une approche basée sur des instances. Chaque objet est représenté par un point dans un espace à n dimensions. Nous définissions les plus proches voisins par une distance euclidienne. Les objets seront classifiés en fonction de leurs voisins les plus proches.

 
Pour créer ces modèles sur knime , il faut tout d’abord créer un workflow (tous les workflows seront mis dans le répertoire analysis sur gitlab) et y ajouter des “Nodes Repository”.  Pour chaque modèle, il faudra ajouter un noeud “File Reader” afin d’y entrer notre matrice avec les paramètres suivant :
- read row IDs
- read column headers
- ignore spaces and tabs
- Column delimiter : tab 

 
Ensuite, pour chaque modèle, il existe un nœud différent avec ses propres paramètres.

Pour le **bayésien naïf ** : Naive Bayes Learner pour le jeu d’apprentissage et le Naive Bayes Predictor pour la prédiction à l’aide du jeu de test. Les paramètres utilisés sont ceux par défaut.

**Arbre de décision ** : Decision Tree Learner et Decision Tree Predictor. Il faudra jongler avec plusieurs paramètre au niveau du Learner :
- Ceux de la mesure de qualité : Gini index et gain ratio
- Ceux de la méthode d’élagage : sans ou avec MDL.
-> l'élagage consiste en l’identification et la suppression des branches correspondant à des exceptions ou du bruit
- Force root split column : On choisis, ou non, de forcer la racine de notre arbre en choisissant les variables qui sont censé classer le mieux d’après nos analyses préliminaire (FD_ID)

**Random Forest ** : Random Forest Learner et Random Forest Predictor. Les paramètres ont été laissés par défaut.

**Knn : ** K Nearest Neighbor. Nous pouvons jouer sur le nombre de voisins k à considérer ( 1, 3 ou 10.) Il faut aussi ajouter un noeud “Normalizer” car la distance calculée par k-nn est en fait sensible aux unités (et plages de valeurs) de chacune des dimensions. Nous avons donc normalisé avec le z-score. 

 
2- Évaluation de ses performances

Il y a deux façons d’évaluer les performances : par validation croisée ou par partitionnement.
Pour la validation croisé, il faut l’ajout de deux noeuds :
X- Partitioner (Partitionne en jeux d’apprentissage et jeux de test)  avec des paramètres à faire varier :
- Number of validations ( 3 ou 10)
- Méthode d'échantillonnage : Linear sampling, random sampling, stratified sampling.
Aggregator  (confronte la classe prédite à la classe connue pour chaque objet testé)

Pour le partitionnement, il faut l’ajout du nœud Partitioning et choisir le pourcentage du jeu d’apprentissage. Nous prenons 66% afin de faire un ⅔ Jeux d’apprentissage, ⅓ Jeux de test.
Enfin, il faut ajouter le nœud Scored ou Statistics afin de regarder le taux d’erreur moyen du modèle. 
Les workflows utilisés sont  :

![workflow](workflow.png)
# Réalisation

Tout d’abord , les résultats du Classificateur Bayésien Naïf  ne sont pas détaillés ci-dessous. En effet, nous obtenons en moyenne un taux d’erreur de 43%. Cela peut s’expliquer par le fait que nos variables ne suivent pas une loi normale et qu’elles ne respectent pas l’hypothèse d’indépendance. En revanche, l’ensemble des résultats obtenus via les autres méthodes en fonction de divers paramètres sont répertoriés dans le tableau récapitulatif ci-dessous


a) Résultat pour Knn

![knn](Resultat_knn.png)

b) Résultat pour Random Forest

![rf](Resultat_random_forest.png)

c) Résultat pour l’arbre de décision

![knn](Resultat_arbre_decision.png)


# Discussion

D’après nos résultats, il semblerait que le Random Forest ait le plus faible taux d’erreur. Nous n’avons cependant pas exploité tous les paramètres possibles pour ce modèle mais les paramètres utilisés semblent tout à fait cohérent avec notre matrice et notre classification, se confirmant par un taux d’erreur relativement “faible” et acceptable.

Les différents arbres de décisions offrent un taux d’erreur toujours inférieur à 4% ce qui est très respectable. Certains paramètres offrent même de très bon résultats comme par exemple Stratified sampling, 10-fold cross validation, sans élagage, avec ou nous forçage de la racine et Gini Index ( 2,7% d’erreur). D’une manière générale, nous obtenons de meilleurs résultats sans élagage et avec le Gini index. En revanche, forcer la racine n’apportent pas forcément de meilleur résultat car notre racine sera par défaut FD_ID ou Q_END, c’est-à-dire les variables expliquant au mieux nos données (comme attendu).

Pour Knn, les taux d’erreurs sont entre 5 et 6%. Il est vrai qu’aux premiers abords, ce taux d’erreur ne semble pas particulièrement aberrant. En revanche, par rapport à nos autres modèles, il semblerait que notre matrice ne soit pas optimisée pour ce modèle comparé aux autres modèles ou bien que nos données ne soient pas adaptées à ce modèle en particulier.

Remarque : la validation croisée leave-one-out n’a pas été effectuée. En effet, avec une matrice de 30 000 observations, cette méthode n'était pas terminée au bout de 6h.


# Bilan et perspectives

Pour conclure quant aux résultats, il semble que cette classification (au vue des taux d’erreurs) soit concluante avec un taux d’erreur respectable.
Donc, dans un premier temps, il semble que la matrice effectuée soit correcte (en nombre d’observations) et que les informations (les variables) contenues par cette dernière sont des variables expliquant relativement bien la répartition des observations dans les 2 classes d’intérêts (ABC ou not_ABC). De plus, il semble que les filtres réalisées ont effectivement permis de réaliser une matrice sans doublons ne comportant que des variables influençant réellement la classification.

Dans un second temps, il semble que les paramètres et les méthodes choisies pour la classification via KNIME sont adéquats et permettent effectivement de classer les observations correctement (taux d’erreur relativement faible).
De plus, d’après l’ensemble des résultats obtenus avec KNIME, il semble que la méthode permettant au mieux de classer nos individus, et qui s’adapte au mieux à nos données et à notre matrice, est le random forest, un prédicteur utilisant la moyenne pour améliorer la précision prédictive et qui contrôle aussi l’ajustement excessif (pouvant trop ajuster le modèle aux données et donc, être trop spécifiques (semblable au sur-apprentissage des réseaux de neurones ?).

En revanche, comme dit précédemment, la méthode utilisant le modèle Bayésien (bien que très souvent utilisée) n’est pas intéressante ici car nos données n’ont pas une distribution qui suit la loi normale.
Ainsi, une perspective à réaliser serait de transformer l’ensemble de nos données contenues dans la matrice finale (via une transformation log par exemple ?) pour peut-être adapter nos données à la méthode de classification Bayésienne et, hypothétiquement, avoir une classification avec un taux d’erreur plus faible que ceux déjà obtenus.

# Gestion du projet

Pour la communication et les rendez-vous, le binôme à utilisé discord comme moyen de communication. Concernant les rendez-vous, ces derniers se sont effectués sur la même application ou bien à la faculté en fonction du travail à effectuer et des disponibilités de chacun. La majorité des tâches ont été réalisées en groupe et les étapes réalisées par un seul membre du binôme ont toujours été vérifiées et validées par le 2ème membre du binôme. Nous avons eu une très bonne communication et les décisions ont toujours été prises ensemble. Même au travers de la difficulté à comprendre et appréhender ce projet, le binôme a su garder une bonne cohésion afin de le mener à bien. Nolan a réalisé le Diagramme de Gantt pour la répartition des tâches au début du projet pour une meilleure organisation lors des séances de travail.

La création de la matrice a été réalisée en grande partie par Nolan étant plus à l’aise sur l’aspect programmation du projet. Gabryelle l’a assistée pour la résolution de bug, les tables de données à choisir, les filtres à réaliser ainsi que l’écriture de la documentation.

Gabryelle à construit les modèles knimes ensuite validés par Nolan. L’écriture des résultats dans les tableaux ont été réalisés par le binôme. Les tableaux ainsi effectués ont ensuite été utilisés pour l’écriture du rapport final du projet.

Gabryelle a écrit une grande partie du rapport (Contexte, Analyse , Conception , Réalisation ) pendant que Nolan a nettoyé et commenté le script sur R pour que ce dernier soit plus lisible et compréhensible. 

Les parties Discussion, Bilan et perspective ainsi que gestion du projet ont été rédigés par les deux membres du binomes. La dernière étape à consisté à organiser le gitlab avec l’ensemble des documents et des codes utilisés tout au long du projet.

Le fichier edt.pdf contient le gantt et se trouve dans ce repertoire. 
